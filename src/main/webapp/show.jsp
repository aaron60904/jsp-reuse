<%-- 
    Document   : show
    Created on : 2022年10月25日, 下午3:36:37
    Author     : user
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <%@include file="/WEB-INF/date.jsp"%>
        <%
            request.setCharacterEncoding("utf-8");
            String date=request.getParameter("date");
            String content=request.getParameter("content");
            content=content.replace("<","&lt;");
            content=content.replace(">","&gt;");
            out.println(date);
            out.println("<br/>");
            out.println(content);
        %>
    </body>
</html>
